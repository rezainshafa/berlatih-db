@extends('layout.master')

@section('judulkanan')
    ShowCast
@endsection

@section('judul')
    Show cast id {{$cast->id}}
@endsection

@section('content')
    <h4> Nama : {{$cast->nama}}</h4>
    <p>Umur : {{$cast->umur}}<br>Biodata : {{$cast->bio}}</p>
    <a href="/cast" class="btn btn-primary">Back</a>
@endsection