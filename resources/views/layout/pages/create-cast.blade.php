@extends('layout.master')

@section('judulkiri')
    Create New Cast
@endsection

@section('judulkanan')
    CreateCast
@endsection

@section('judul')
    New Cast
@endsection

@section('content')
    <form action="/cast" method="POST">
        @csrf
    <div class="card-body">
      <div class="form-group">
        <label>NAMA LENGKAP</label>
        <input type="text" class="form-control" name="name">
        @error('name')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
        @enderror
      </div>
      <div class="form-group">
        <label>UMUR</label>
        <input type="number" class="form-control" name="umur">
        @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
        @enderror
      </div>
      <div class="form-group">
        <label>BIODATA</label>
        <textarea class="form-control" rows="3" placeholder="Enter Biodata Here" name="bio"></textarea>
        @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
      </div>
      <div class="form-group">
        <button type="submit" class="btn btn-primary">Create</button>
      </div>
    </div>
    <!-- /.card-body -->
  </form>
@endsection

