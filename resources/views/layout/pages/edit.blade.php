@extends('layout.master')

@section('judulkanan')
    EditCast
@endsection

@section('judul')
    Edit & Update Cast {{$cast->id}}
@endsection

@section('content')
    <div>
    <form action="/cast/{{$cast->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="card-body">
            <div class="form-group">
              <label>NAMA LENGKAP</label>
              <input type="text" class="form-control" value="{{$cast->nama}}" name="name">
              @error('name')
                          <div class="alert alert-danger">
                              {{ $message }}
                          </div>
              @enderror
            </div>
            <div class="form-group">
              <label>UMUR</label>
              <input type="number" class="form-control" value="{{$cast->umur}}" name="umur">
              @error('umur')
                          <div class="alert alert-danger">
                              {{ $message }}
                          </div>
              @enderror
            </div>
            <div class="form-group">
              <label>BIODATA</label>
              <textarea class="form-control" rows="3" placeholder="Enter Biodata Here" value="{{$cast->bio}}" name="bio"></textarea>
              @error('bio')
                          <div class="alert alert-danger">
                              {{ $message }}
                          </div>
                      @enderror
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
            <a href="/cast" class="btn btn-primary">Batal</a>
        </div>
        
    </form>
</div>
@endsection
