<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//page starter
Route::get('/', function () {
    return view('welcome');
});

//page Admin
Route::get('/admin', function () {
    return view('layout.master');
});

Route::get('/login', function () {
    return view('layout.pages.login');
});

Route::get('/table', function () {
    return view('layout.pages.table');
});

Route::get('/data-tables', function () {
    return view('layout.pages.data-tables');
});


//page databases
Route::get('/cast','CastContoller@index');
Route::get('/cast/create','CastContoller@create');
Route::post('/cast','CastContoller@store');
Route::get('/cast/{cast_id}','CastContoller@show');
Route::get('/cast/{cast_id}/edit','CastContoller@edit');
Route::put('/cast/{cast_id}','CastContoller@update');
Route::delete('/cast/{cast_id}','CastContoller@destroy');


