<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastContoller extends Controller
{
    public function index() {
        $cast = DB::table('cast')->get();
        return view('layout.pages.index-cast', compact('cast'));
    }

    public function create() {
        return view('layout.pages.create-cast');
    }

    public function store(Request $request) {
        $request->validate([
            'name' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);
        $query = DB::table('cast')->insert([
            "nama" => $request["name"],
            "umur" => $request["umur"],
            "bio" => $request["bio"]
        ]);
        return redirect('/cast');
    }

    public function show($id) {
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('layout.pages.show', compact('cast'));
    }

    public function edit($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('layout.pages.edit', compact('cast'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'name' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);

        $query = DB::table('cast')
            ->where('id', $id)
            ->update([
                "nama" => $request["name"],
                "umur" => $request["umur"],
                "bio" => $request["bio"]
            ]);
        return redirect('/cast');
    }

    public function destroy($id)
    {
        $query = DB::table('cast')->where('id', $id)->delete();
        return redirect('/cast');
    }
}


